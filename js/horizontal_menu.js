/**
 * Created by vasa on 17.03.14.
 */

(function ($) {
  "use strict";

  $(function () {
    var $header = $('header.header-horizontal'),
      $subHeaders = $header.find('.sub-header-block'),
      $menuButtons = $header.find('a[data-horizontal-menu-toggle]'),
      $menuButtonWrappers = $menuButtons.parent(),
      $menuHeaders = $header.find('.sub-header-row'),
      $closer = $header.find('.close'),
      $window = $(window);

    $header.on('hh:close-all', function () {
      $menuButtonWrappers.removeClass('menu-active');
      $header.find('div.sub-header-block:visible').hide('blind', 'fast');
    });

    $menuButtons.on('click', function (evt) {

      evt.preventDefault();

      var $this = $(this),
        $parent = $this.parent(),
        $menu = $header.find('#' + $this.data('horizontal-menu-toggle'));

      if ($parent.hasClass('menu-active')) {
        $parent.removeClass('menu-active');
        $this.blur();
        $menu.hide('blind', 'fast', function () {
          $header.trigger('hh:hidden-menu');
        });
      } else {
        $menuButtonWrappers.removeClass('menu-active');
        $parent.addClass('menu-active');

        var $visible_headers = $header.find('.sub-header-block:visible');
        if ($header.find('.sub-header-block:visible').length) {
          $header.trigger('hh:show-menu');
          $visible_headers.hide();
          $menu.show();
        } else {
          $header.trigger('hh:show-menu');
          $menu.show('blind', 'fast');
        }
      }
    });

    $menuHeaders.on('click', function (evt) {
      if ($window.width() <= 768 && $(evt.target).is(":not(a)")) {

        var $this = $(this),
          $menu = $this.find('.sub-header-row-right');

        if ($menu.is(':visible')) {
          $menu.hide();
        } else {
          $this.closest('.sub-header-block').find('.sub-header-row-right').hide();
          $menu.show();
        }
      }
    });

    $closer.on('click', function () {
      if ($window.width() <= 768) {
        $menuButtons.parent().removeClass('menu-active');
        $subHeaders.hide();
      }
    });
  });

})(jQuery);